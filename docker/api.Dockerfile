FROM node:8

WORKDIR /usr/src/app
COPY . .

ENV PORT 3000
ARG DOCKER_ENV=development
ENV NODE_ENV=${DOCKER_ENV}

RUN npm install

EXPOSE ${PORT}
CMD [ "node", "./server.js" ]
