FROM maven

WORKDIR /sources

COPY . .

VOLUME /sources/.mvn

# Force output filename
RUN sed -i 's/<build>/<build><finalName>output<\/finalName>/' pom.xml

EXPOSE 8091
CMD mvn package && java -jar target/output.jar;