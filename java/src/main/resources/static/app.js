var stompClient = null;

function setConnected(connected) {
	$('#call').removeClass('btn-secondary btn-success');
	if (connected) {
		$("#connect").hide();
		$("#disconnect").show();
        $("#interactions").show();
    	$('#call').addClass('btn-success').addClass('focus');
	} else {
		$("#connect").show();
		$("#disconnect").hide();
		$('#call').addClass('btn-secondary');
	}
    $('#call').prop('disabled', !connected);
    updateStatus();
}

function connect() {
    var socket = new SockJS('/almundo-ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/log', function (rawLog) {
        	let log = JSON.parse(rawLog.body);
            showLogs(log.level, log.description);
        });
        stompClient.subscribe('/topic/status', function (rawStatus) {
        	let status = JSON.parse(rawStatus.body);
            setCallers(status);
        });
    }, function(message) {
    	setConnected(false);
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function makeCall() {
	$('#call').removeClass('focus');	
    stompClient.send("/almundo/incoming");
}
function updateStatus() {
    stompClient.send("/almundo/status");
}

function showLogs(level, description) {
	let icon = 'info';
	if (level == 'WARN')
		icon = 'phone-volume';
    $("#logs").append('<div class="row mt-3 text-center text-' + level.toLowerCase() + '"><div class="col-sm-2 fas fa-' + icon + '"></div><div class="col-sm-10 text-lg-left">' + description + '</div></div>')
    		.stop().animate({ scrollTop: $("#logs")[0].scrollHeight}, 700);
}
function setCallers(status) {
    let calls = '';
    for (let i = 0; i < status.countOperators; i++) {
    	calls += '<i class="fas fa-4x fa-user-cog text-success" style="margin: 15px;"></i>';
	}
    $('#operators').html(calls);
    calls = '';
    for (let i = 0; i < status.countSupervisors; i++) {
    	calls += '<i class="fas fa-5x fa-user-edit text-primary" style="margin: 15px;"></i>';
	}
    $('#supervisors').html(calls);
    calls = '';
    for (let i = 0; i < status.countDirectors; i++) {
    	calls += '<i class="fas fa-6x fa-user-tie text-warning" style="margin: 15px;"></i>';
	}
    $('#directors').html(calls);
    calls = '';
    for (let i = 0; i < status.waitingCallers; i++) {
    	calls += '<i class="fas fa-7x fa-user-clock text-danger" style="margin: 15px;"></i>';
	}
    $('#waiters').html(calls);
    
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#call" ).click(function() { makeCall(); });
    
    $('#logs, #calls').css({'height': $(window).height() - $('#logs').offset().top - 30})
    
    connect();
});