package com.almundo.examen.models;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.almundo.examen.config.LoggerConfig;
import com.almundo.examen.config.NotificationConfig;

/**
 * Handles incoming calls.
 * 
 * @author lucasr
 *
 */
@Configuration
public class Dispatcher implements Runnable {

	private ExecutorService operators;
	private ExecutorService supervisors;
	private ExecutorService directors;
	private CallsStatus status;
	
	@Autowired
	private LoggerConfig logger;
	@Autowired
	private NotificationConfig notificator;
	
	/**
	 * @return the wired instance for the notificator
	 */
	public NotificationConfig getNotificator() {
		return notificator;
	}
	/**
	 * @return the wired instance for the logging
	 */
	public LoggerConfig getLogger() {
		return logger;
	}

	/**
	 * @return the singleton instance for the status
	 */
	public CallsStatus getStatus() {
		return status;
	}

	/**
	 * @param status required from the calls controller
	 */
	public void setStatus(CallsStatus status) {
		this.status = status;
	}

	/**
	 * Wired constructor with parameters from the environment.
	 * 
	 * @param operatorsAmount is the number of available Operators. Wired from <i>operators.amount</i> 
	 * @param supervisorsAmount is the number of available Supervisors. Wired from <i>supervisors.amount</i>
	 * @param directorsAmount is the number of available Directors. Wired from <i>directors.amount</i>
	 * @param callTimeout is the maximum time allowed on a call in seconds. Wired from <i>call.timeoutSeconds</i>
	 */
	@Autowired
	public Dispatcher(@Value("${operators.amount}") int operatorsAmount,
			@Value("${supervisors.amount}") int supervisorsAmount,
			@Value("${directors.amount}") int directorsAmount,
			@Value("${call.timeoutSeconds}") int callTimeout
			) {

		this.operators = generateFixedPool(operatorsAmount, callTimeout);
		this.supervisors = generateFixedPool(supervisorsAmount, callTimeout);
		this.directors = generateFixedPool(directorsAmount, callTimeout);
	}
	
	/**
	 * Custom implementation of a thread pool.
	 * It has a fixed size and when there is none available it throws an RejectedExecutionException.
	 * 
	 * @param size of the pool
	 * @param timeout for the wait of a thread to be idle
	 * @return the configured hard-fixed pool
	 */
	private ExecutorService generateFixedPool(int size, int timeout) {
		final SynchronousQueue<Runnable> voidQueue = new SynchronousQueue<>();
		
		return new ThreadPoolExecutor(
				size, size,
				timeout, TimeUnit.SECONDS,
				voidQueue);

	}
	
	/**
	 * Dispatch to the correct employee the incoming call according to a predefined order.
	 * If no-one is available, wait 1s and try again.
	 * <pre>Dar alguna solución sobre qué pasa con una llamada cuando no hay ningún empleado libre.</pre>
	 */
	public void dispatchCall() {
	    try {
	    	operators.execute(new Operator(this));
	    } catch (RejectedExecutionException e) {
		    try {
		    	supervisors.execute(new Supervisor(this));
		    } catch (RejectedExecutionException e1) {
				try {
					directors.execute(new Director(this));
				} catch (RejectedExecutionException e2) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e3) {
						e1.printStackTrace();
					}
					dispatchCall();			
				}
			}
		}
	}

	@Override
	public void run() {
		logger.logInfo("in dispatcher");
		status.addDispatcher();
		dispatchCall();
	}
	
	@Override
	protected void finalize() throws Throwable {
		status.delDispatcher();
		super.finalize();
	}	
}
