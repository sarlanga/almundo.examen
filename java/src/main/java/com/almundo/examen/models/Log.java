package com.almundo.examen.models;

import org.springframework.boot.logging.LogLevel;

/**
 * Log model to be used as an interface.
 * 
 * @author lucasr
 *
 */
public class Log {
	private LogLevel level;
	private String description;

	/**
	 * @param level is the level of the message
	 * @param description is the messate to log
	 */
	public Log(LogLevel level, String description) {
		this.level = level;
		this.description = description;
	}

	/**
	 * @return the level of the log
	 */
	public LogLevel getLevel() {
		return level;
	}
	/**
	 * @return the message of the log
	 */
	public String getDescription() {
		return description;
	}
}
