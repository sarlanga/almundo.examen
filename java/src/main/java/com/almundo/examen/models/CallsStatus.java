package com.almundo.examen.models;

/**
 * General status model. Contains and syncronize all the counters.
 * 
 * @author lucasr
 *
 */
public class CallsStatus {
	private int operators=0;
	private int supervisors=0;
	private int directors=0;
	private int callers=0;
	private int dispatchers = 0;

	public CallsStatus() {
	}
	
	/**
	 * <strong>Synchronously</strong> add 1 to the running Operators counter.
	 */
	public synchronized void addOperator() {
		operators++;
	}
	/**
	 * <strong>Synchronously</strong> subtracts 1 to the running Operators counter.
	 */
	public synchronized void delOperator() {
		operators--;	
		callers--;
	}
	/**
	 * <strong>Synchronously</strong> add 1 to the running Supervisors counter.
	 */
	public synchronized void addSupervisor() {
		supervisors++;	
	}
	/**
	 * <strong>Synchronously</strong> subtracts 1 to the running Supervisors counter.
	 */
	public synchronized void delSupervisor() {
		supervisors--;	
		callers--;
	}
	/**
	 * <strong>Synchronously</strong> add 1 to the running Directors counter.
	 */
	public synchronized void addDirector() {
		directors++;	
	}
	/**
	 * <strong>Synchronously</strong> subtracts 1 to the running Supervisors counter.
	 */
	public synchronized void delDirector() {
		directors--;
		callers--;
	}
	/**
	 * <strong>Synchronously</strong> add 1 to the current Callers counter.
	 */
	public synchronized void addCaller() {
		callers++;	
	}
	/**
	 * <strong>Synchronously</strong> add 1 to the running Dispatchers counter.
	 */
	public synchronized void addDispatcher() {
		dispatchers++;	
	}
	/**
	 * <strong>Synchronously</strong> subtracts 1 to the running Dispatchers counter.
	 */
	public synchronized void delDispatcher() {
		dispatchers--;
	}
	/**
	 * Obtains the count of the running Operators.
	 * 
	 * @return the Operators count
	 */
	public int getCountOperators() {
		return operators;
	}
	/**
	 * Obtains the count of the running Supervisors.
	 * 
	 * @return the Supervisors count
	 */
	public int getCountSupervisors() {
		return supervisors;
	}
	/**
	 * Obtains the count of the running Directors.
	 * 
	 * @return the Directors count
	 */
	public int getCountDirectors() {
		return directors;
	}
	/**
	 * Obtains the count of the active Callers.
	 * 
	 * @return the Callers count
	 */
	public int getCountCallers() {
		return callers;
	}
	/**
	 * Obtains the count of the active Dispatchers.
	 * 
	 * @return the Dispatchers count
	 */
	public int getCountDispatchers() {
		return dispatchers;
	}
	/**
	 * Obtains the count of the Calls being processed.
	 * 
	 * @return count of Calls
	 */
	public int getTotalWorkers() {
		return this.getCountOperators() + this.getCountSupervisors() + this.getCountDirectors();
	}
	/**
	 * Obtains the count of the Calls not yet processed.
	 * 
	 * @return count of Calls
	 */
	public int getWaitingCallers() {
		return this.getCountCallers() - this.getTotalWorkers();
	}
}
