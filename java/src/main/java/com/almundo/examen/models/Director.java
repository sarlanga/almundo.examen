package com.almundo.examen.models;

/**
 * Model of the Director level of Employee.
 * Overrides the Runnable implementation of Employee to add custom behavior.
 * 
 * @author lucasr
 *
 */
public class Director extends Employee {
	Dispatcher dispatcher;
	
	/**
	 * Constructor for the model of Director.
	 * Requires the Dispatcher to sync notifications and counter.
	 * 
	 * @param dispatcher required for notifications and counters
	 */
	public Director(Dispatcher dispatcher) {
		super(dispatcher.getLogger());
		this.dispatcher = dispatcher;
	}
	
	@Override
	public void run() {
		logger.logInfo("Director Answering");
		CallsStatus status = dispatcher.getStatus();
		status.addDirector();
		dispatcher.getNotificator().notifyStatus(status);
		super.run();
		status.delDirector();
		dispatcher.getNotificator().notifyStatus(status);
	}
}
