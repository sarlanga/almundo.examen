package com.almundo.examen.models;

/**
 * Model of the Operator level of Employee.
 * Overrides the Runnable implementation of Employee to add custom behavior.
 * 
 * @author lucasr
 *
 */

public class Operator extends Employee
{
	Dispatcher dispatcher;
	
	/**
	 * Constructor for the model of Director.
	 * Requires the Dispatcher to sync notifications and counter.
	 * 
	 * @param dispatcher required for notifications and counters
	 */
	public Operator(Dispatcher dispatcher) {
		super(dispatcher.getLogger());
		this.dispatcher = dispatcher;
	}

	@Override
	public void run() {
		logger.logInfo("Operator Answering");
		CallsStatus status = dispatcher.getStatus();
		status.addOperator();
		dispatcher.getNotificator().notifyStatus(status);
		super.run();
		status.delOperator();
		dispatcher.getNotificator().notifyStatus(status);
	}
}
