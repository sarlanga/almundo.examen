package com.almundo.examen.models;

import java.util.Random;

import com.almundo.examen.config.LoggerConfig;

/**
 * Basic Employee model to handle calls.
 * 
 * @author lucasr
 *
 */
public abstract class Employee implements Runnable {

	LoggerConfig logger;
	Random random = new Random();
	
	/**
	 * @param logger is wired and is required for logging 
	 */
	public Employee(LoggerConfig logger) {
		this.logger = logger;
	}

	@Override
	public void run() {
		try {
			logger.logWarn("WORKING WITH CALL");

			int maxDuration = 10;
			int minDuration = 5;
			int callDuration = random.nextInt(maxDuration - minDuration + 1) + minDuration; 
			Thread.sleep(callDuration * 1000);		
			
			logger.logWarn("ENDING CALL (" + callDuration + "s)");
		} catch (Exception e) {
			System.err.println();
		}
	}
}
