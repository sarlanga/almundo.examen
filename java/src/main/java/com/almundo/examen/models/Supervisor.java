package com.almundo.examen.models;

/**
 * Model of the Supervisor level of Employee.
 * Overrides the Runnable implementation of Employee to add custom behavior.
 * 
 * @author lucasr
 *
 */
public class Supervisor extends Employee
{
	Dispatcher dispatcher;
	
	/**
	 * Constructor for the model of Director.
	 * Requires the Dispatcher to sync notifications and counter.
	 * 
	 * @param dispatcher required for notifications and counters
	 */
	public Supervisor(Dispatcher dispatcher) {
		super(dispatcher.getLogger());
		this.dispatcher = dispatcher;
	}
	
	@Override
	public void run() {
		logger.logInfo("Supervisor Answering");
		CallsStatus status = dispatcher.getStatus();
		status.addSupervisor();
		dispatcher.getNotificator().notifyStatus(status);
		super.run();
		status.delSupervisor();
		dispatcher.getNotificator().notifyStatus(status);
	}
}
