package com.almundo.examen.controllers;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import com.almundo.examen.config.LoggerConfig;
import com.almundo.examen.models.CallsStatus;
import com.almundo.examen.models.Dispatcher;

/**
 * Main consolidator of the websockets messages. 
 * 
 * @author lucasr
 *
 */
@Controller
public class CallController {

	/**
	 * Pool for simultaneous dispatchers set to 10. Queue the rest.
	 * <pre><h3>Dar alguna solución sobre qué pasa con una llamada cuando entran
más de 10 llamadas concurrentes.</h3></pre>
	 */
	private final ExecutorService dispatcherPool = Executors.newFixedThreadPool(10);
	@Autowired
	private Dispatcher dispatcher;

	@Autowired
    LoggerConfig logger;
	
    static CallsStatus status = new CallsStatus();
	
	/**
	 * Websocket endpoint to receive a new call.
	 */
	@MessageMapping("/incoming")
	public void receiveIncommingCall() {
		logger.logInfo("INCOMING CALL!!");

		status.addCaller();
		dispatcher.setStatus(status);
		dispatcher.getNotificator().notifyStatus(status);
		
		dispatcherPool.execute(dispatcher);
	}
	/**
	 * Websocket endpoint to synchronously get the current general status.
	 * 
	 * @return the general status on /topic/status
	 */
	@MessageMapping("/status")
	@SendTo("/topic/status")
	public CallsStatus getStatus() {
		return status;
	}
}
