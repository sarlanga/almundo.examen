package com.almundo.examen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *	Spring APP for the Al Mundo test.
 *	Uses WebSockets as external interface.
 *
 * @author lucasr
 * 
 */
@SpringBootApplication
public class ExamenApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamenApplication.class, args);
	}
}
