package com.almundo.examen.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import com.almundo.examen.models.CallsStatus;

/**
 * Notifications distributor.
 * 
 * @author lucasr
 *
 */
@Component
public class NotificationConfig {
    @Autowired
    SimpMessagingTemplate template;

    /**
     * Sends the general status to the websocket.
     * Uses /topic/status
     * @param status is the general status
     */
    public void notifyStatus(CallsStatus status) {
    	template.convertAndSend("/topic/status", status);
    }
}
