package com.almundo.examen.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.logging.LogLevel;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import com.almundo.examen.models.Log;

/**
 * Logger distributor.
 * 
 * @author lucasr
 *
 */
@Component
public class LoggerConfig {
    @Autowired
    SimpMessagingTemplate template;
    
    /**
     * If property enabled, the logs are also send to stderr for debugging.
     */
    @Value("${log.useConsole}")
    private boolean useConsole;

    /**
     * Logs high level information to the websocket.
     * Uses /topic/log
     * @param info is the text to log
     */
    public void logInfo(String info) {
    	template.convertAndSend("/topic/log", new Log(LogLevel.INFO, info));
    	if (useConsole)
    		System.err.println("------------ " + info + " ------------");
    }
    /**
     * Logs low level information to the websocket.
     * Uses /topic/log
     * @param info is the text to log
     */
    public void logWarn(String info) {
    	template.convertAndSend("/topic/log", new Log(LogLevel.WARN, info));
    	if (useConsole)
    		System.err.println("------------ " + info + " ------------");
    }
}
