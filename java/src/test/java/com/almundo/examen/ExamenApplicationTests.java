package com.almundo.examen;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ClassUtils;
import org.springframework.util.StopWatch;

import com.almundo.examen.controllers.CallController;
import com.almundo.examen.models.Dispatcher;
import com.almundo.examen.models.Employee;

/**
 * @author lucasr
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ExamenApplicationTests {

	@Autowired
	CallController callController;
	@Autowired
	Dispatcher dispatcher;

	/**
	 * Find all the available classes from current package.
	 * @return list of classes
	 */
	private ArrayList<String> findClasses() {
		ArrayList<String> retval = new ArrayList<String>();
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		MetadataReaderFactory readerFactory = new CachingMetadataReaderFactory(resolver);
		String basePath = ClassUtils.convertClassNameToResourcePath(this.getClass().getPackage().getName());
		Resource[] resources;
		try {
		    resources = resolver.getResources("classpath*:" + basePath + "/**/*.class");
		} catch (IOException e) {
		    throw new AssertionError(e);
		}
		for (Resource resource : resources) {
		    MetadataReader reader;
		    try {
		        reader = readerFactory.getMetadataReader(resource);
		    } catch (IOException e) {
		        throw new AssertionError(e);
		    }
		    String className = reader.getClassMetadata().getClassName();
		    retval.add(className);   
		}
		return retval;
	}
	
	/**
	 * Debe existir una clase Dispatcher encargada de manejar las
		llamadas, y debe contener el método dispatchCall para que las
		asigne a los empleados disponibles.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testExistsDispatcher() {
		Class dispatcherClass = null;
		Method dispatchCallMethod = null;
		try {
			Optional<String> fullClassName = findClasses().stream().filter(c -> c.contains("Dispatcher")).findFirst();
			if (fullClassName.isPresent()) {
				dispatcherClass = Class.forName(fullClassName.get());
				dispatchCallMethod = dispatcherClass.getMethod("dispatchCall");
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		assertThat(dispatcherClass.getSimpleName()).isEqualTo("Dispatcher");
		assertThat(dispatchCallMethod.getName()).isEqualTo("dispatchCall");
	}
	
	/**
	 * La clase Dispatcher debe tener la capacidad de poder procesar 10
	    llamadas al mismo tiempo (de modo concurrente).
	    
	 * @throws InterruptedException if all gets stopped
	 */
	@Test
	public void testDispatcherCapacity() throws InterruptedException {
		int massAmount = 20;
		ExecutorService executor = Executors.newFixedThreadPool(massAmount);
		executor.invokeAll(Collections.nCopies(massAmount, new Callable<Boolean>() {		
			@Override
			public Boolean call() throws Exception {
				callController.receiveIncommingCall();
				Thread.sleep(500); // Warmup time
				return null;
			}
		}));
		System.out.println(callController.getStatus().getCountDispatchers());
		assertThat(callController.getStatus().getCountDispatchers()).isGreaterThanOrEqualTo(10);
		executor.shutdownNow();
	}
	
	/**
	 * El método dispatchCall puede invocarse por varios hilos al mismo
		tiempo.
	 * @throws InterruptedException if all gets stopped
	 */
	@Test
	public void testDispatcherCallCapacity() throws InterruptedException {
		boolean executionResult = false;
		int massAmount = 5;
		ExecutorService executor = Executors.newFixedThreadPool(massAmount);
		try {
			executionResult = executor.invokeAny(Collections.nCopies(massAmount, (Callable<Boolean>) () -> {
				dispatcher.dispatchCall();
				Thread.sleep(500); // Warmup time
				return true;
			}));
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		assertThat(executionResult).isTrue();
		executor.shutdownNow();		
	}
	
	/**
	 * Cada llamada puede durar un tiempo aleatorio entre 5 y 10
		segundos.
	 * @throws InterruptedException if all gets stopped
	 */
	@Test
	public void testCallDuration() throws InterruptedException {
	      StopWatch timer = new StopWatch();
	      Employee employee = new Employee(dispatcher.getLogger()) {};
	      timer.start();
	      employee.run();
	      timer.stop();
	      assertThat((int)timer.getTotalTimeSeconds()).isGreaterThanOrEqualTo(5).isLessThanOrEqualTo(10);
	}
}
