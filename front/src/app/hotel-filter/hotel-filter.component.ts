import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faSearch, faStar, faSortUp, faSortDown } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-hotel-filter',
  templateUrl: './hotel-filter.component.html',
  styleUrls: ['./hotel-filter.component.scss']
})
export class HotelFilterComponent implements OnInit {

  @Output() filterEvent: EventEmitter<Filter> = new EventEmitter<Filter>();
  filter: Filter = {name: null, stars: []};

  Arr = Array;  // Only to do the for 1..n
  faSearch = faSearch;
  faSortUp = faSortUp;
  faSortDown = faSortDown;
  faStar = faStar;
  starsList: Array<StarsFilter> = [
  		{ id: 0, status: ''},
  		{ id: 5, status: ''},
  		{ id: 4, status: ''},
  		{ id: 3, status: ''},
  		{ id: 2, status: ''},
  		{ id: 1, status: ''}
  		];
  filterDisplay: FilterDisplay = {full: window.screen.width >= 768, name: true, stars: true};
  waitBuffer = null;

  constructor() { }

  ngOnInit() {
  }

  onNameChange(name: string) {
  	this.filter.name = name;
  	if(this.filter.name.length < 3)
  		this.filter.name = null;
  	
  }

  onStarsChange(stars: StarsFilter) {
	let starIdx = this.filter.stars.indexOf(stars.id);
  	if (starIdx > -1) {
  		stars.status = '';
  	} else {
  		stars.status = 'on';
  	}
  	if (stars.id) {
	  	if(starIdx > -1) {
	  		this.filter.stars.splice(starIdx, 1);
	  		this.starsList[0].status = '';
	  	} else {
	  		this.filter.stars.push(stars.id);
	  	}
  	} else if (stars.status === 'on') {
  		this.filter.stars = [];
  		this.starsList.forEach(value => {
  			if (value.id) {
	  			value.status = 'on';
	  			this.filter.stars.push(value.id);			
  			}
  		});
  	}
  	this.updateFilter();
  }

  updateFilter() {
  	let that = this;
	clearTimeout(that.waitBuffer);
  	that.waitBuffer = setTimeout(function() {
		that.filterEvent.emit(that.filter);
  	}, 200);
  }

  toggleName() {
  	this.filterDisplay.name = !this.filterDisplay.name;
  }
  toggleStars() {
  	this.filterDisplay.stars = !this.filterDisplay.stars;
  }
  toggleFull() {
  	this.filterDisplay.full = !this.filterDisplay.full;
  }
}

interface FilterDisplay {
	full: boolean;
	name: boolean;
	stars: boolean;
}

export interface Filter {
    name?: string;
    stars?: Array<number>;
}

export interface StarsFilter {
	id: number;
	status: string;
}
