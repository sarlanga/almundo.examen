import { registerLocaleData } from '@angular/common';
import { Component, OnInit, Input } from '@angular/core';
import es from '@angular/common/locales/es';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.scss']
})
export class HotelComponent implements OnInit {

  @Input() hotel: Hotel;
  Arr = Array;  // Only to do the for 1..n
  constructor() { }

  ngOnInit() {
    registerLocaleData( es );
  }
}

export interface Hotel {
    id: number;
    name: string;
    stars: number;
    price: number;
    image: string;
    amenities: Array<string>;
}
