import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

import { HotelComponent, Hotel } from '../hotel/hotel.component';
import { HotelFilterComponent, Filter } from '../hotel-filter/hotel-filter.component';

@Component({
  selector: 'app-hotel-list',
  templateUrl: './hotel-list.component.html',
  styleUrls: ['./hotel-list.component.scss']
})
export class HotelListComponent implements OnInit {

  hotels: Array<Hotel>;
  constructor(private api: ApiService) { }

  ngOnInit() {
    this.api.getHotels()
      .subscribe(res => {
        console.log(res);
        this.hotels = res;
      }, err => {
        console.log(err);
      });
  }

  onFilter(filter: Filter) {
    this.api.getHotels(filter.name, filter.stars)
      .subscribe(res => {
        this.hotels = res;
      }, err => {
        console.log(err);
      });  	
  }
}
