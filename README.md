# Al Mundo (Examen LRancez)

## API
NodeJS REST CRUD API.
Definición en /api/swagger.yml

*El puerto default es el 3000.*

## FRONT
Angular 6 con Bootstrap 4.

*El puerto default es el 4200.*

## Java
ThreadPool custom con una interfaz montada con WebSockets de Spring.

*El puerto default es el 8091.*

---
## DOCKER
Ejecutando /docker/docker-compose.yml se crea una mongo db, una instancia para api node, para front y una para la app en java.


Las distintas imágenes pueden montarse por separado usando:
```
$ docker-compose -f docker/docker-compose.yml up api
```
```
$ docker-compose -f docker/docker-compose.yml up front
```
```
$ docker-compose -f docker/docker-compose.yml up java
```

MongoDB tiene los datos iniciales en /mongo-seed/data.json; puede inicializarze mediante este comando:
```
$ docker-compose -f docker/docker-compose.yml exec data mongoimport --host mongodb --db hotels --collection hotels --type json --file /mongo-seed/data.json --jsonArray
```
