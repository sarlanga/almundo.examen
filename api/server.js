const express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  bodyParser = require('body-parser'),
  queryParser = require('express-query-int');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(queryParser());

let routes = require('./api/routes/hotels.routes');
routes(app);

app.listen(port);

console.log('API server started on: ' + port);

module.exports = app; // for testing
