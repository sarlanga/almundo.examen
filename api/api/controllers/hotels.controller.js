'use strict';
const Hotels = require('../models/hotels.model');

/*
exports.list_filtered = function(req, res, next) {
  setCORS();

  let result = Hotels.get_all();
  if (req.query.name)
  	result = Hotels.filter_by_name(req.query.name, result);
  if (req.query.stars)
  	result = Hotels.filter_by_stars(req.query.stars, result);

  res.json(result);
};
*/

let respondHotels = function(data, ctx) {
  ctx.res.header('Access-Control-Allow-Origin', 'http://localhost:4200');
  ctx.res.header('Access-Control-Allow-Credentials', 'true');
  ctx.res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  ctx.res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

  ctx.res.send(data);
};

exports.list_filtered = function(req, res, next) {

  if (req.query.name || req.query.stars)
  	Hotels.filter({name: req.query.name, stars: req.query.stars}, respondHotels, {res: res});
  else
	Hotels.get_all(respondHotels, {res: res});
};

exports.find_by_id = function(req, res, next) {
	Hotels.get_by_id(req.params.id, respondHotels, {res: res});
};

exports.create = function(req, res, next) {
	Hotels.create(req.body, respondHotels, {res: res});
};
exports.update_by_id = function(req, res, next) {
	Hotels.update_by_id(req.params.id, req.body, respondHotels, {res: res});
};
exports.delete_by_id = function(req, res, next) {
	Hotels.delete_by_id(req.params.id, respondHotels, {res: res});
};