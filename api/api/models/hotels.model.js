'use strict';

// Configuring the database
const dbConfig = require('../config/database.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...');
    process.exit();
});


const HotelSchema = mongoose.Schema({
    id: Number,
    name: String,
    stars: Number,
    price: Number,
    image: String,
    amenities: Array
});

let HotelModel = mongoose.model('Hotel', HotelSchema);

/*
const hotels = require('../../resources/data/data.json');

module.exports.get_all = function() { return hotels };

module.exports.filter_by_name = function (name, items) {
	if (!items)
		items = get_all();

	return items.filter(hotel => {
		return hotel.name.toLowerCase().indexOf(name.toLowerCase()) > -1;
	})
};

module.exports.filter_by_stars = function (stars, items) {
	if (!items)
		items = get_all();

	return items.filter(hotel => {
		return stars.indexOf(hotel.stars) > -1;
	})
};
*/

module.exports.get_all = function(cb, ctx) { HotelModel.find().exec((err, hotels) => {
	cb(hotels, ctx);
}) };

module.exports.filter = function(query, cb, ctx) {
	let mongoQuery = HotelModel;
	if (query.name) {
		mongoQuery = mongoQuery.find({'name': new RegExp(query.name.replace(/\s+/g,"\\s+"), "gi")});
	}
	if (query.stars) {
		mongoQuery = mongoQuery.where('stars').in(JSON.parse(query.stars));
		console.log(mongoQuery);
	}
	mongoQuery.exec((err, hotels) => {
		cb(hotels, ctx);
	})
};

/*
module.exports.get_by_id = function(id, cb, ctx) { 
	HotelModel.find({'id': id}).exec((err, hotels) => {

	cb(hotels, ctx);
}) };
*/

module.exports.get_by_id = function(id, cb, ctx) {
	this.get_all(function(hotels) {
		cb(hotels.filter(hotel => hotel.id == id)[0], ctx)
	});
};

module.exports.create = function(body, cb, ctx) {
	let hotel = new HotelModel({
		id: body.id,
		name: body.name,
		stars: body.stars,
		price: body.price,
		image: body.image,
		amenities: body.amenities
	}).save().then((hotels) => {
		cb(hotels, ctx);
	});
};

module.exports.update_by_id = function(id, body, cb, ctx) {
	this.get_by_id(id, hotel => {
		hotel.name = body.name || hotel.name;
		hotel.stars = body.stars || hotel.stars;
		hotel.price = body.price || hotel.price;
		hotel.image = body.image || hotel.image;
		hotel.amenities = body.amenities || hotel.amenities;
		hotel.save().then((hotel) => {
			cb(hotel, ctx);
		})
	});
};

module.exports.delete_by_id = function(id, cb, ctx) {
	this.get_by_id(id, hotel => {
		hotel.remove().then((hotel) => {
			cb(hotel, ctx);
		})
	});
};
