'use strict';
module.exports = function(app) {
  const hotelsController = require('../controllers/hotels.controller');

  app.route('/hotels')
    .get(hotelsController.list_filtered);

  app.route('/hotels')
    .post(hotelsController.create);

  app.route('/hotels/:id')
    .get(hotelsController.find_by_id);

  app.route('/hotels/:id')
    .put(hotelsController.update_by_id);

  app.route('/hotels/:id')
    .delete(hotelsController.delete_by_id);

  app.options("/*", function(req, res, next){
    res.header('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    res.send(200);
  });

  app.use(function(req, res) {
    res.status(404).send({url: req.originalUrl + ' not found'})
  });

};
