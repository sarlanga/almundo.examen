//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let Hotels = require('../api/models/hotels.model');

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

chai.use(chaiHttp);

describe('Hotels', () => {
  describe('/GET hotels', () => {
    it('it should GET all the hotels', (done) => {
      chai.request(server)
          .get('/hotels')
          .end((err, res) => {
                res.should.have.status(200);
                res.body.should.have.property('hotels');
                res.body.hotels.should.be.a('array');
                res.body.hotels.length.should.be.not.eql(0);
            done();
          });
    });
  });
});
